﻿using System.Configuration;

namespace Sb.Configuration
{
    public class WebConfigurationEnvironment : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name
        {
            get { return this["name"] as string; }
        }

        [ConfigurationProperty("inheritFrom", IsRequired = false)]
        public string InheritFrom
        {
            get { return this["inheritFrom"] as string; }
        }

        [ConfigurationProperty("settings")]
        public WebConfigurationSettings Settings
        {
            get
            {
                return this["settings"] as WebConfigurationSettings;
            }
        }
    }
}
