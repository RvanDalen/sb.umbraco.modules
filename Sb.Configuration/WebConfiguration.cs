﻿using System;
using System.Configuration;
using System.Web;

namespace Sb.Configuration 
{
    public class WebConfiguration : ConfigurationSection
    {
        [ConfigurationProperty("environments")]
        [ConfigurationCollection(typeof(WebConfigurationEnvironments), AddItemName = "environment")]
        public WebConfigurationEnvironments Environments
        {
            get
            {
                return this["environments"] as WebConfigurationEnvironments;
            }
        }

        public WebConfigurationEnvironment CurrentEnvironment
        {
            get
            {
                if (Environments[HttpContext.Current.Server.MachineName] == null)
                    throw new Exception(string.Format("This environment '{0}' not properly configured", HttpContext.Current.Server.MachineName));
                return Environments[HttpContext.Current.Server.MachineName];
            }
        }

        public WebConfigurationEnvironment GetEnvironment(string name)
        {
            if (Environments[HttpContext.Current.Server.MachineName] == null)
                throw new Exception(string.Format("Could not find configuration for '{0}'", name));
            return Environments[name];
        }
    }
}
