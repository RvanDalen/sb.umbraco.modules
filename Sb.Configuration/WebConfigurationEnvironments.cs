﻿using System.Configuration;

namespace Sb.Configuration
{
    public class WebConfigurationEnvironments : ConfigurationElementCollection
    {
        public new WebConfigurationEnvironment this[string key]
        {
            get
            {
                return BaseGet(key) as WebConfigurationEnvironment;
            }
            set
            {
                if (BaseGet(key) != null)
                {
                    BaseRemove(key);
                }
                BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new WebConfigurationEnvironment();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((WebConfigurationEnvironment)element).Name;
        }
    }
}
