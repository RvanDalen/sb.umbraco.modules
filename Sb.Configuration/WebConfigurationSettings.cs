﻿using System.Configuration;

namespace Sb.Configuration
{
    public class WebConfigurationSettings : ConfigurationElementCollection
    {
        public new WebConfigurationSetting this[string key]
        {
            get
            {
                return BaseGet(key) as WebConfigurationSetting;
            }
            set
            {
                if (BaseGet(key) != null)
                {
                    BaseRemove(key);
                }
                BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new WebConfigurationSetting();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((WebConfigurationSetting)element).Key;
        } 
    }
}
