﻿using System;
using System.ComponentModel;
using System.Linq;

namespace Sb.Commons.Helpers
{
    public static class EnumHelper
    {
        public static string GetDescription(Enum value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());
            var description = fieldInfo.GetCustomAttributes(typeof (DescriptionAttribute), false).FirstOrDefault() as DescriptionAttribute;
            return description != null ? description.Description : value.ToString();
        }
    }
}
