﻿using System.Net;
using System.Net.Mail;
using BoC.InversionOfControl;
using Sb.Commons.Providers;

namespace Sb.Commons.Helpers
{
    public static class MailHelper
    {
        public static void SendMail(MailMessage message)
        {
            var configurationService = IoC.Resolver.Resolve<IConfigurationProvider>();
            var mailServer = configurationService.SmtpServer;
            SmtpClient smtpClient;
            if (string.IsNullOrEmpty(mailServer))
            {
                smtpClient = new SmtpClient();
            }
            else
            {
                var mailServerPort = configurationService.SmtpPort;
                smtpClient = mailServerPort <= 0 ? new SmtpClient(mailServer) : new SmtpClient(mailServer, mailServerPort);
            }
            var mailServerUserName = configurationService.SmtpUser;
            if (mailServerUserName.Length > 0)
            {
                var mailServerPassword = configurationService.SmtpPassword;
                var networkCredential = new NetworkCredential(mailServerUserName, mailServerPassword);
                smtpClient.Credentials = networkCredential;
            }
            smtpClient.Send(message);
        }
    }
}
