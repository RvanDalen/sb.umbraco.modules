﻿using System.Collections.Generic;
using System.Net.Mail;

namespace Sb.Commons.Models
{
    public interface IEmailTemplate
    {
        string Subject { get; set; }
        string Sender { get; set; }
        IEnumerable<string> Recipients { get; set; }
        string Body { get; set; }
    }
}
