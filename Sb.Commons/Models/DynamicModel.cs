﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;

namespace Sb.Commons.Models
{
    public class DynamicModel : DynamicObject
    {
        private readonly IDictionary<string, object> _dict = (IDictionary<string, object>)new Dictionary<string, object>();

        /// <summary>
        /// Gets the set of dynamic member names.
        /// 
        /// </summary>
        /// 
        /// <returns>
        /// An instance of <see cref="T:System.Collections.Generic.IEnumerable`1"/>.
        /// </returns>
        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return _dict.Keys;
        }

        /// <summary>
        /// Attempts to read a dynamic member from the object.
        /// 
        /// </summary>
        /// <param name="binder">The binder.</param><param name="result">The result instance.</param>
        /// <returns>
        /// True, always.
        /// </returns>
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            var key = binder.Name.ToUpper();
            result = !this._dict.ContainsKey(key) ? (object)null : this._dict[key];
            return true;
        }

        /// <summary>
        /// Attempts to set a value on the object.
        /// 
        /// </summary>
        /// <param name="binder">The binder.</param><param name="value">The value to set.</param>
        /// <returns>
        /// True, always.
        /// </returns>
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            var key = binder.Name.ToUpper();
            if (this._dict.ContainsKey(key))
                this._dict[key] = value;
            else
                this._dict.Add(key, value);
            return true;
        }

        /// <summary>
        /// Add a value to this instance of DynamicViewBag.
        /// 
        /// </summary>
        /// <param name="propertyName">The property name through which this value can be get/set.
        ///             </param><param name="value">The value that will be assigned to this property name.
        ///             </param>
        public void AddValue(string propertyName, object value)
        {
            if (propertyName == null)
                throw new ArgumentNullException("The propertyName parameter may not be NULL.");
            if (this._dict.ContainsKey(propertyName.ToUpper()))
                throw new ArgumentException("Attempt to add duplicate value for the '" + propertyName + "' property.");
            this._dict.Add(propertyName.ToUpper(), value);
        }

        /// <summary>
        /// Adds values from the specified valueList to this instance of DynamicViewBag.
        /// 
        /// </summary>
        /// <param name="valueList">A list of objects.  Each must have a public property of keyPropertyName.
        ///             </param><param name="keyPropertyName">The property name that will be retrieved for each object in the specified valueList
        ///             and used as the key (property name) for the ViewBag.  This property must be of type string.
        ///             </param>
        public void AddListValues(IList valueList, string keyPropertyName)
        {
            foreach (object obj1 in (IEnumerable)valueList)
            {
                if (obj1 == null)
                    throw new ArgumentNullException("Invalid NULL value in initializer list.");
                object obj2 = (object)obj1.GetType().GetProperty(keyPropertyName);
                if (obj2.GetType() != typeof(string))
                    throw new ArgumentNullException("The keyPropertyName property must be of type string.");
                string key = (string)obj2;
                if (this._dict.ContainsKey(key.ToUpper()))
                    throw new ArgumentException("Attempt to add duplicate value for the '" + key + "' property.");
                this._dict.Add(key.ToUpper(), obj1);
            }
        }

        /// <summary>
        /// Adds values from the specified valueDictionary to this instance of DynamicViewBag.
        /// 
        /// </summary>
        /// <param name="valueDictionary">A dictionary of objects.  The Key of each item in the dictionary will be used
        ///             as the key (property name) for the ViewBag.
        ///             </param>
        public void AddDictionaryValues(IDictionary valueDictionary)
        {
            foreach (object obj1 in (IEnumerable)valueDictionary.Keys)
            {
                if (obj1.GetType() != typeof(string))
                    throw new ArgumentNullException("The Key in valueDictionary must be of type string.");
                string key = ((string)obj1);
                if (this._dict.ContainsKey(key.ToUpper()))
                    throw new ArgumentException("Attempt to add duplicate value for the '" + key + "' property.");
                object obj2 = valueDictionary[(object)key];
                this._dict.Add(key.ToUpper(), obj2);
            }
        }

        /// <summary>
        /// Adds values from the specified valueDictionary to this instance of DynamicViewBag.
        /// 
        /// </summary>
        /// <param name="valueDictionary">A generic dictionary of {string, object} objects.  The Key of each item in the
        ///             dictionary will be used as the key (property name) for the ViewBag.
        ///             </param>
        /// <remarks>
        /// This method was intentionally not overloaded from AddDictionaryValues due to an ambiguous
        ///             signature when the caller passes in a Dictionary&lt;string, object&gt; as the valueDictionary.
        ///             This is because the Dictionary&lt;TK, TV&gt;() class implements both IDictionary and IDictionary&lt;TK, TV&gt;.
        ///             A Dictionary&lt;string, ???&gt; (any other type than object) will resolve to AddDictionaryValues.
        ///             This is specifically for a generic List&lt;string, object&gt;, which does not resolve to
        ///             an IDictionary interface.
        /// 
        /// </remarks>
        public void AddDictionaryValuesEx(IDictionary<string, object> valueDictionary)
        {
            foreach (string key in (IEnumerable<string>)valueDictionary.Keys)
            {
                if (this._dict.ContainsKey(key))
                    throw new ArgumentException("Attempt to add duplicate value for the '" + key + "' property.");
                object obj = valueDictionary[key];
                this._dict.Add(key.ToUpper(), obj);
            }
        }
    }
}
