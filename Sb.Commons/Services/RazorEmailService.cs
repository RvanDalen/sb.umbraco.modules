﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using RazorEngine;
using Sb.Commons.Models;

namespace Sb.Commons.Services
{
    public class RazorEmailService : EmailService
    {
        public override MailMessage BuildMailMessage(IEmailTemplate mailTemplate, object values)
        {
            var dynamicModel = new DynamicModel();
            dynamicModel.AddDictionaryValuesEx(AsDictionary(values));

            var msg = new MailMessage();

            msg.From = new MailAddress(Parse(mailTemplate.Sender, dynamicModel));
            foreach (var recipient in mailTemplate.Recipients)
            {
                msg.To.Add(Parse(recipient, dynamicModel));
            }

            msg.Subject = Parse(mailTemplate.Subject, dynamicModel);
            msg.IsBodyHtml = true;
            msg.Body = Parse(mailTemplate.Body, dynamicModel);

            return msg;
        }

        private static IDictionary<string, object> AsDictionary(object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            if (source is IDictionary<string, object>)
                return (IDictionary<string, object>)source;
            return source.GetType().GetProperties(bindingAttr).ToDictionary
            (
                propInfo => propInfo.Name,
                propInfo => propInfo.GetValue(source, null)
            );
        }

        private string Parse(string template, object model)
        {
            try
            {
                return Razor.Parse(template, model);
            }
            catch (Exception)
            {
                return template;
            }
        }
    }
}
