﻿using System.Net.Mail;
using Sb.Commons.Models;

namespace Sb.Commons.Services
{
    public interface IEmailService
    {
        bool Send(IEmailTemplate mailTemplate, object values);
        MailMessage BuildMailMessage(IEmailTemplate mailTemplate, object values);
    }
}