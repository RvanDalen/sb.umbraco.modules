﻿using System;
using System.Net.Mail;
using System.Net.Sockets;
using Sb.Commons.Helpers;
using Sb.Commons.Models;

namespace Sb.Commons.Services
{
    public abstract class EmailService : IEmailService
    {
        public virtual bool Send(IEmailTemplate mailTemplate, object values)
        {
            MailMessage msg = null;

            try
            {
                msg = BuildMailMessage(mailTemplate, values);

                MailHelper.SendMail(msg);
                return true;
            }
            catch (SocketException)
            {
                try
                {
                    if (msg == null) msg = BuildMailMessage(mailTemplate, values);

                    MailHelper.SendMail(msg);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public abstract MailMessage BuildMailMessage(IEmailTemplate mailTemplate, object values);
    }
}
