﻿using System.Web.Mvc;

namespace Sb.Commons.Mvc
{
    public class EmptyPartialViewResult : PartialViewResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            //do nothing
        }
    }
}
