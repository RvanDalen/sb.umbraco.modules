﻿namespace Sb.Commons.Providers
{
    public interface IConfigurationProvider
    {
        string GetSetting(string key);
        string SmtpServer { get; }
        int SmtpPort { get; }
        string SmtpUser { get; }
        string SmtpPassword { get; }
    }
}