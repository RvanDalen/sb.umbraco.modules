﻿using System;
using System.Configuration;
using Sb.Configuration;

namespace Sb.Commons.Providers
{
    public class ConfigurationProvider : IConfigurationProvider
    {
        private WebConfiguration GetConfig()
        {
            try
            {
                return (WebConfiguration)ConfigurationManager.GetSection("sb.WebConfiguration");
            }
            catch (Exception ex)
            {
                throw new Exception("Invalid configuration", ex);
            }
        }

        private string GetSetting(string key, string environment)
        {
            if (GetConfig().GetEnvironment(environment).Settings[key] != null)
            {
                return GetConfig().GetEnvironment(environment).Settings[key].Value;
            }

            //go to fallback environment
            if (!string.IsNullOrEmpty(GetConfig().GetEnvironment(environment).InheritFrom))
            {
                return GetSetting(key, GetConfig().GetEnvironment(environment).InheritFrom);
            }

            return string.Empty;
        }

        public string GetSetting(string key)
        {
            return GetSetting(key, GetConfig().CurrentEnvironment.Name);
        }

        #region defaults

        public string SmtpServer
        {
            get { return GetSetting("SmtpServer"); }
        }

        public int SmtpPort
        {
            get
            {
                int port;
                return int.TryParse(GetSetting("SmtpServer"), out port) ? port : 25;
            }
        }

        public string SmtpUser
        {
            get { return GetSetting("SmtpUser"); }
        }

        public string SmtpPassword
        {
            get { return GetSetting("SmtpPassword"); }
        }

        #endregion
    }
}
