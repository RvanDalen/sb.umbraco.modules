﻿using System;
using Sb.Commons.Helpers;

namespace Sb.Commons.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum value)
        {
            return EnumHelper.GetDescription(value);
        }
    }
}
