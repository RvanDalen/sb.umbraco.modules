﻿using BoC.InversionOfControl;
using BoC.Tasks;
using Sb.Commons.Providers;
using Sb.Commons.Services;

namespace Sb.Commons.Init
{
    public class RegisterServices : IBootstrapperTask
    {
        private readonly IDependencyResolver _resolver;

        public RegisterServices(IDependencyResolver resolver)
        {
            _resolver = resolver;
        }

        public void Execute()
        {
            _resolver.RegisterType<IEmailService, RazorEmailService>();
            _resolver.RegisterType<IConfigurationProvider, ConfigurationProvider>();
        }
    }
}
