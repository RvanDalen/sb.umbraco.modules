﻿using System;
using Sb.Umbraco.Glass.ArcheType;
using Sb.Umbraco.Glass.Helpers;

namespace Sb.Umbraco.Glass.Extensions
{
    public static class ArcheTypeExtensions
    {
        public static string GetItemPropertyValue(this ArcheTypeItem item, string propertyAlias)
        {
            return ArcheTypeHelper.GetItemPropertyValue(item, propertyAlias);
        }

        public static T GetItemPropertyValue<T>(this ArcheTypeItem item, string propertyAlias)
        {
            var stringValue = GetItemPropertyValue(item, propertyAlias);
            try
            {
                if (typeof (T).IsEnum)
                {
                    int intValue;
                    int.TryParse(stringValue, out intValue);
                    if (intValue == -1) intValue = 0;

                    return (T)Enum.ToObject(typeof (T), intValue);
                }

                var value = Convert.ChangeType(stringValue, typeof (T));
                return (T) value;
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}
