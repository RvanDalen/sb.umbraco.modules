﻿using System.Linq;
using BoC.InversionOfControl;
using BoC.Persistence.UmbracoGlass;
using Glass.Mapper.Umb.Configuration;

namespace Sb.Umbraco.Glass.Extensions
{
    public static class StringExtensions
    {
        public static UmbracoTypeConfiguration GetUmbracoTypeConfiguration(this string documentTypeAlias)
        {
            var umbracoService = IoC.Resolver.Resolve<IUmbracoServiceProvider>().GetUmbracoService();

            return umbracoService.GlassContext.TypeConfigurations.FirstOrDefault(tc =>
            {
                var umbTc = tc.Value as UmbracoTypeConfiguration;
                return umbTc != null && !string.IsNullOrEmpty(umbTc.ContentTypeAlias) && umbTc.ContentTypeAlias.Equals(documentTypeAlias);
            }).Value as UmbracoTypeConfiguration;
        }
    }
}
