﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Sb.Umbraco.Glass.ArcheType;
using Sb.Umbraco.Glass.Extensions;
using Umbraco.Web.Templates;

namespace Sb.Umbraco.Glass.Helpers
{
    public static class ArcheTypeHelper
    {
        public static IEnumerable<BaseArcheTypeModel> GetModels(string json)
        {
            return GetModels<BaseArcheTypeModel>(json);
        }

        public static IEnumerable<T> GetModels<T>(string json) where T : BaseArcheTypeModel
        {
            if (string.IsNullOrEmpty(json)) return null;

            var archeType = JsonConvert.DeserializeObject<ArcheTypeContainer>(json);
            var models = new List<T>();

            foreach (var archeTypeItem in archeType.Items)
            {
                var umbracoConfiguration = archeTypeItem.Alias.GetUmbracoTypeConfiguration();

                if (umbracoConfiguration != null && typeof (T).IsAssignableFrom(umbracoConfiguration.Type))
                {
                    var model = Activator.CreateInstance(umbracoConfiguration.Type, archeTypeItem) as T;
                    if (model != null && model.ArcheTypeItem.Properties.Any(prop => !string.IsNullOrEmpty(prop.Value))) models.Add(model);
                }
                    
            }

            return models;
        }

        public static string GetItemPropertyValue(ArcheTypeItem item, string propertyAlias)
        {
            var property = item.Properties.FirstOrDefault(prop => prop.Alias.Equals(propertyAlias));
            return property != null ? TemplateUtilities.ParseInternalLinks(property.Value) : null;
        }
    }
}
