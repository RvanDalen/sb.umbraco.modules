﻿using Sb.Umbraco.Glass.Models;

namespace Sb.Umbraco.Glass.Helpers
{
    public static class BaseCommonHelper
    {
        public static string GetUrl(this BaseCommon baseCommon)
        {
            var niceUrl = umbraco.library.NiceUrl(baseCommon.NodeId);
            if (baseCommon.Parent == null) niceUrl = string.Format("/{0}/", baseCommon.Name.ToLowerInvariant());
            return niceUrl;
        }
    }
}
