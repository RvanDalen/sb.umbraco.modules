﻿namespace Sb.Umbraco.Glass.ArcheType
{
    public abstract class BaseArcheTypeModel
    {
        public ArcheTypeItem ArcheTypeItem { get; private set; }
        public string Alias { get; set; }

        public BaseArcheTypeModel(ArcheTypeItem archeTypeItem)
        {
            if (archeTypeItem == null) return;

            ArcheTypeItem = archeTypeItem;
            Alias = archeTypeItem.Alias;
        }
    }
}
