﻿using Newtonsoft.Json;

namespace Sb.Umbraco.Glass.ArcheType
{
    public class ArcheTypeProperty
    {
        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }
}