﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Sb.Umbraco.Glass.ArcheType
{
    public class ArcheTypeContainer
    {
        [JsonProperty("fieldsets")]
        public List<ArcheTypeItem> Items { get; set; }
    }
}
