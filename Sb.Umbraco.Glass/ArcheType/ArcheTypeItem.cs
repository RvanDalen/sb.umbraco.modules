﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Sb.Umbraco.Glass.ArcheType
{
    public class ArcheTypeItem
    {
        [JsonProperty("alias")]
        public string Alias { get; set; }

        [JsonProperty("properties")]
        public List<ArcheTypeProperty> Properties { get; set; }
    }
}