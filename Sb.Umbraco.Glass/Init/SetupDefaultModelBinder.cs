﻿using System.Web.Mvc;
using BoC.Tasks;
using Sb.Umbraco.Glass.Mvc;

namespace Sb.Umbraco.Glass.Init
{
    public class SetupDefaultModelBinder : IBootstrapperTask
    {
        public void Execute()
        {
            ModelBinders.Binders.DefaultBinder = new UmbracoGlassModelBinder();
        }
    }
}
