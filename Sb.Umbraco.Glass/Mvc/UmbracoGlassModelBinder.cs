﻿using System.Web.Mvc;
using BoC.InversionOfControl;
using BoC.Services;
using Sb.Umbraco.Glass.Models;
using Umbraco.Web;

namespace Sb.Umbraco.Glass.Mvc
{
    public class UmbracoGlassModelBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (typeof(ICommon).IsAssignableFrom(bindingContext.ModelType))
            {
                var modelService = IoC.Resolver.Resolve<IModelService<BaseCommon>>();
                return modelService.Get(UmbracoContext.Current.PageId);
            }

            return base.BindModel(controllerContext, bindingContext);
        }
    }
}
