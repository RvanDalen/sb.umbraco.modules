﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using BoC.Helpers;
using BoC.InversionOfControl;
using Glass.Mapper.Umb;
using Glass.Mapper.Umb.Configuration;
using Sb.Umbraco.Glass.Extensions;
using Sb.Umbraco.Glass.Models;
using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Web.Mvc;
using UmbracoContext = Umbraco.Web.UmbracoContext;

namespace Sb.Umbraco.Glass.Mvc
{
    public class UmbracoGlassControllerFactory : UmbracoControllerFactory
    {
        public override bool CanHandle(RequestContext request)
        {
            if (UmbracoContext.Current == null || UmbracoContext.Current.PublishedContentRequest == null) return false;
            if (String.CompareOrdinal(request.RouteData.DataTokens["umbraco"] as string, "surface") == 0) return false;
            
            var requestedDocumentTypeAlias = UmbracoContext.Current.PublishedContentRequest.PublishedContent.DocumentTypeAlias;
            return  requestedDocumentTypeAlias.GetUmbracoTypeConfiguration() != null;;
        }

        public override IController CreateController(RequestContext requestContext, string controllerName)
        {
            var appDomainHelper = IoC.Resolver.Resolve<IAppDomainHelper>();
            var requestedDocumentTypeAlias = UmbracoContext.Current.PublishedContentRequest.PublishedContent.DocumentTypeAlias;
            var requestedType = GetGlassType(requestedDocumentTypeAlias);
            var allowedTypes = GetGlassTypes(requestedType);
            var controllerType = appDomainHelper.GetTypes(t => t.BaseType != null
                                                            && t.BaseType.IsGenericType
                                                            && t.BaseType.GetGenericTypeDefinition() == typeof(UmbracoGlassController<>)
                                                            && t.BaseType.GetGenericArguments().Any(gt => gt != null && allowedTypes.Contains(gt))
                                                         ).OrderBy(t => allowedTypes.IndexOf(t.BaseType.GetGenericArguments().FirstOrDefault())).FirstOrDefault();

            try
            {
                var controller = (IController)Activator.CreateInstance(controllerType);
                var controllerBase = controller as Controller;
                controllerBase.ActionInvoker = new UmbracoGlassControllerActionInvoker();

                requestContext.RouteData.Values["action"] = requestedDocumentTypeAlias;
                requestContext.RouteData.Values["controller"] = controllerType.Name.Replace("Controller", "");

                if (requestContext.RouteData.DataTokens.ContainsKey("umbraco"))
                    requestContext.RouteData.DataTokens.Remove("umbraco");

                return controller;
            }
            catch (Exception)
            {
                //TODO: log a not found warning
            }
            return base.CreateController(requestContext, controllerName);
        }

        public override Type GetControllerType(RequestContext requestContext, string controllerName)
        {
            var appDomainHelper = IoC.Resolver.Resolve<IAppDomainHelper>();
            var requestedDocumentTypeAlias = UmbracoContext.Current.PublishedContentRequest.PublishedContent.DocumentTypeAlias;
            var requestedType = GetGlassType(requestedDocumentTypeAlias);
            var allowedTypes = GetGlassTypes(requestedType);
            var controllerType = appDomainHelper.GetTypes(t => t.BaseType != null
                                                            && t.BaseType.IsGenericType
                                                            && t.BaseType.GetGenericTypeDefinition() == typeof(UmbracoGlassController<>)
                                                            && t.BaseType.GetGenericArguments().Any(gt => gt != null && allowedTypes.Contains(gt))
                                                         ).OrderBy(allowedTypes.IndexOf).FirstOrDefault();

            return controllerType ?? base.GetControllerType(requestContext, controllerName);
        }

        //TODO: move this into an extension method
        private Type GetGlassType(string documentTypeAlias)
        {
            var service = new UmbracoService(new ContentService());
            var config = service.GlassContext.TypeConfigurations.FirstOrDefault(tc =>
            {
                var umbTc = tc.Value as UmbracoTypeConfiguration;
                return umbTc != null && !string.IsNullOrEmpty(umbTc.ContentTypeAlias) && umbTc.ContentTypeAlias.Equals(documentTypeAlias);
            });
            return config.Key;
        }

        private List<Type> GetGlassTypes(Type requestedType)
        {
            return requestedType.GetBaseTypes(true)
                                .Where(t => typeof(BaseCommon).IsAssignableFrom(t))
                                .ToList();
        }
    }
}
