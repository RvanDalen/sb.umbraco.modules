﻿using System;
using Umbraco.Core.ObjectResolution;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace Sb.Umbraco.Glass.Mvc
{
    public class UmbracoGlassApplication : UmbracoApplication
    {
        protected override void OnApplicationStarting(object sender, EventArgs e)
        {
            ResolverBase<FilteredControllerFactoriesResolver>.Current.InsertType<UmbracoGlassControllerFactory>(0);
            base.OnApplicationStarting(sender, e);
        }
    }
}
