﻿using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.UI;

namespace Sb.Umbraco.Glass.Mvc
{
    public class UmbracoGlassControllerActionInvoker : ControllerActionInvoker
    {
        protected override ActionDescriptor FindAction(ControllerContext controllerContext, ControllerDescriptor controllerDescriptor, string actionName)
        {
            var action = base.FindAction(controllerContext, controllerDescriptor, actionName);
            var baseType = controllerContext.Controller.GetType().BaseType;
            if (action == null && baseType != null &&
                baseType.GetGenericTypeDefinition() == typeof (UmbracoGlassController<>))
            {
                //reset action to Index
                controllerContext.RouteData.Values["action"] = "Index";

                MethodInfo cleanActionMethod = null;
                MethodInfo filterActionMethod = null;

                foreach (var methodInfo in controllerContext.Controller.GetType().GetMethods(BindingFlags.Public|BindingFlags.DeclaredOnly|BindingFlags.Instance))
                {
                    if (methodInfo.Name == "Index")
                    {
                        var selectorAttributes = methodInfo.GetCustomAttributes(typeof(ActionMethodSelectorAttribute), false) as ActionMethodSelectorAttribute[];

                        if (!methodInfo.GetCustomAttributes(typeof (NonActionAttribute), false).Any() && (selectorAttributes == null || (!selectorAttributes.Any())))
                            cleanActionMethod = methodInfo;
                        else if (selectorAttributes != null && selectorAttributes.Any(selector => selector.IsValidForRequest(controllerContext, methodInfo)))
                            filterActionMethod = methodInfo;
                    }
                }

                return new ReflectedActionDescriptor(filterActionMethod ?? cleanActionMethod, "Index", controllerDescriptor);
            }
            return action;
        }
    }
}
