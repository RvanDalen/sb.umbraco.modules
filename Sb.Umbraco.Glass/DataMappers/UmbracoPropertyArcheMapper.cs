﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Glass.Mapper;
using Glass.Mapper.Configuration;
using Glass.Mapper.Umb;
using Glass.Mapper.Umb.Configuration;
using Glass.Mapper.Umb.DataMappers;
using Sb.Umbraco.Glass.ArcheType;
using Sb.Umbraco.Glass.Helpers;

namespace Sb.Umbraco.Glass.DataMappers
{
    public class UmbracoPropertyArcheMapper : AbstractUmbracoPropertyMapper
    {
        public override object SetPropertyValue(object value, UmbracoPropertyConfiguration config, UmbracoDataMappingContext context)
        {
            throw new NotImplementedException();
        }

        public override object GetPropertyValue(object propertyValue, UmbracoPropertyConfiguration config, UmbracoDataMappingContext context)
        {
            var type = config.PropertyInfo.PropertyType;
            var argumentType = global::Glass.Mapper.Umb.Utilities.GetGenericArgument(type);
            var list = global::Glass.Mapper.Utilities.CreateGenericType(typeof(List<>), new[] { argumentType }) as IList;

            foreach (var item in ArcheTypeHelper.GetModels(propertyValue as string).Where(argumentType.IsInstanceOfType))
            {
                list.Add(item);
            }

            return list;
        }

        public override bool CanHandle(AbstractPropertyConfiguration configuration, Context context)
        {
            var type = configuration.PropertyInfo.PropertyType;
            if (!type.IsGenericType || type.GetGenericTypeDefinition() != typeof(IEnumerable<>)) return false;

            var argumentType = global::Glass.Mapper.Umb.Utilities.GetGenericArgument(type);
            return typeof(BaseArcheTypeModel).IsAssignableFrom(argumentType);
        }
    }
}
