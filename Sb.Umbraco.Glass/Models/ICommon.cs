﻿using System;
using System.Collections.Generic;
using BoC.Persistence;
using Glass.Mapper.Umb.Configuration;
using Glass.Mapper.Umb.Configuration.Attributes;

namespace Sb.Umbraco.Glass.Models
{
    [UmbracoType(AutoMap = true)]
    public interface ICommon : IBaseEntity<Guid>
    {
        [UmbracoId]
        Guid Id { get; set; }

        [UmbracoId]
        int NodeId { get; set; }

        [UmbracoInfo(UmbracoInfoType.Name)]
        string Name { get; set; }

        [UmbracoInfo(UmbracoInfoType.Path)]
        string Path { get; set; }

        [UmbracoChildren(InferType = true, IsLazy = true)]
        IEnumerable<ICommon> Children { get; set; }

        [UmbracoParent(InferType = true, IsLazy = true)]
        ICommon Parent { get; set; }
    }
}
