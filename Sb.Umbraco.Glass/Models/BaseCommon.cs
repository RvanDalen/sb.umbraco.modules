﻿using System;
using System.Collections.Generic;
using BoC.Persistence;
using Glass.Mapper.Umb.Configuration.Attributes;

namespace Sb.Umbraco.Glass.Models
{
    [UmbracoType(AutoMap = true)]
    public class BaseCommon : BaseEntity<Guid>, ICommon
    {
        public override Guid Id { get; set; }
        public virtual int NodeId { get; set; }
        public virtual string Name { get; set; }
        public virtual string Path { get; set; }
        public virtual IEnumerable<ICommon> Children { get; set; }
        public virtual ICommon Parent { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
