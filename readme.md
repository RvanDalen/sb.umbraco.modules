# README #

My SimpleBreeze modules solution mainly for Umbraco. It provides a robust framework with providers, services and helpers like:
- ConfigurationProvider
- EmailService
- ErrorService
- RedirectService
- FolderService
- SettingService