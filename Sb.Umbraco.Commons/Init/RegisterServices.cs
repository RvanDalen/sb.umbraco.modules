﻿using BoC.InversionOfControl;
using BoC.Tasks;
using Sb.Umbraco.Commons.Services;

namespace Sb.Umbraco.Commons.Init
{
    public class RegisterServices : IBootstrapperTask
    {
        private readonly IDependencyResolver _resolver;

        public RegisterServices(IDependencyResolver resolver)
        {
            _resolver = resolver;
        }

        public void Execute()
        {
            _resolver.RegisterType<IErrorService, ErrorService>();
            _resolver.RegisterType<IFolderService, FolderService>();
            _resolver.RegisterType<IRedirectService, RedirectService>();
        }
    }
}
