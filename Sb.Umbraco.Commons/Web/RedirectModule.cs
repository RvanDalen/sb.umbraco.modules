﻿using System;
using System.Web;
using BoC.InversionOfControl;
using Sb.Umbraco.Commons.Enums;
using Sb.Umbraco.Commons.Services;
using Sb.Umbraco.Glass.Helpers;

namespace Sb.Umbraco.Commons.Web
{
    public class RedirectModule : IHttpModule
    {
        public void Dispose()
        {
            
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += BeginRequest;
        }

        private void BeginRequest(object sender, EventArgs e)
        {
            var application = (HttpApplication)sender;
            var context = application.Context;

            var redirectService = IoC.Resolver.Resolve<IRedirectService>();
            var redirect = redirectService.FindMatch(context.Request.Url);

            if (redirect == null) return;

            var targetUrl = redirect.Target.GetUrl();
            if (string.IsNullOrEmpty(targetUrl)) return;

            var host = context.Request.Url.Host;
            if (context.Request.Url.Port != 80 && context.Request.Url.Port != 443)
                host = string.Format("{0}:{1}", host, context.Request.Url.Port);
            targetUrl = string.Format("{0}://{1}{2}", context.Request.Url.Scheme, host, targetUrl);

            if (redirect.Status == HttpStatus.MovedPermenantly) context.Response.RedirectPermanent(targetUrl);
            context.Response.Redirect(targetUrl);
        }
    }
}
