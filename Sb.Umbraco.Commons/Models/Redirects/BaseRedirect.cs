﻿using Glass.Mapper.Umb.Configuration;
using Glass.Mapper.Umb.Configuration.Attributes;
using Sb.Umbraco.Commons.Enums;
using Sb.Umbraco.Glass.Models;

namespace Sb.Umbraco.Commons.Models.Redirects
{
    [UmbracoType(AutoMap = true, ContentTypeAlias = "BaseRedirect")]
    public class BaseRedirect : BaseCommon
    {
        [UmbracoProperty("status", UmbracoPropertyType.NotSet, "Settings")]
        public virtual HttpStatus Status { get; set; }

        [UmbracoProperty("target", UmbracoPropertyType.ContentPicker, "Settings")]
        public virtual BaseCommon Target { get; set; }
    }
}
