﻿using Glass.Mapper.Umb.Configuration;
using Glass.Mapper.Umb.Configuration.Attributes;

namespace Sb.Umbraco.Commons.Models.Redirects
{
    [UmbracoType(AutoMap = true, ContentTypeAlias = "PathRedirect")]
    public class PathRedirect : BaseRedirect
    {
        [UmbracoProperty("path", UmbracoPropertyType.TextString, "Settings")]
        public virtual string UrlPath { get; set; }
    }
}
