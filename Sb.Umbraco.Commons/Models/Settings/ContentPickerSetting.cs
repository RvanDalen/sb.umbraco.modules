﻿using Glass.Mapper.Umb.Configuration;
using Glass.Mapper.Umb.Configuration.Attributes;
using Sb.Umbraco.Glass.Models;

namespace Sb.Umbraco.Commons.Models.Settings
{
    [UmbracoType(AutoMap = true, ContentTypeAlias = "ContentPickerSetting")]
    public class ContentPickerSetting : BaseSetting
    {
        [UmbracoProperty("moduleFolder", UmbracoPropertyType.ContentPicker, "Settings")]
        public virtual BaseCommon PageItem { get; set; }

        public override object Value
        {
            get { return PageItem; }
            set { PageItem = (BaseCommon)value; }
        }
    }
}
