﻿using Glass.Mapper.Umb.Configuration.Attributes;
using Sb.Umbraco.Glass.Models;

namespace Sb.Umbraco.Commons.Models.Settings
{
    [UmbracoType(AutoMap = true, ContentTypeAlias = "BaseSetting")]
    public abstract class BaseSetting : BaseCommon
    {
        public virtual object Value { get; set; }
    }
}
