﻿using Glass.Mapper.Umb.Configuration.Attributes;
using Sb.Umbraco.Glass.Models;

namespace Sb.Umbraco.Commons.Models.Common
{
    [UmbracoType(AutoMap = true, ContentTypeAlias = "FolderData")]
    public class FolderData : BaseCommon
    {
    }
}
