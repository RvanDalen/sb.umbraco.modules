﻿using System;
using System.Web;
using System.Web.Mvc;
using BoC.InversionOfControl;
using Sb.Umbraco.Commons.Services;
using Sb.Umbraco.Glass.Helpers;

namespace Sb.Umbraco.Commons.Mvc
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class GlassHandleErrorAttribute : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            if (filterContext == null) throw new ArgumentNullException("filterContext");
            if (filterContext.IsChildAction || filterContext.ExceptionHandled || !filterContext.HttpContext.IsCustomErrorEnabled) return;

            var exception = filterContext.Exception;
            var httpException = new HttpException(null, exception);
            var httpCode = httpException.GetHttpCode();
            if (new HttpException(null, exception).GetHttpCode() != 500 || !ExceptionType.IsInstanceOfType(exception)) return;

            //var controllerName = (string)filterContext.RouteData.Values["controller"];
            //var actionName = (string)filterContext.RouteData.Values["action"];
            //todo log something!

            var errorService = IoC.Resolver.Resolve<IErrorService>();
            var page = errorService.GetFriendlyErrorPage(httpCode);
            var exceptionContext = filterContext;

            exceptionContext.Result = new RedirectResult(page.GetUrl());
            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = 500;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }
    }
}
