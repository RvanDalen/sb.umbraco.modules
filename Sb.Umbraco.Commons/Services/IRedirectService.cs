﻿using System;
using BoC.Services;
using Sb.Umbraco.Commons.Models.Redirects;

namespace Sb.Umbraco.Commons.Services
{
    public interface IRedirectService : IModelService<BaseRedirect>
    {
        BaseRedirect FindMatch(Uri uri);
    }
}