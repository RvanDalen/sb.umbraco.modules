using Sb.Umbraco.Glass.Models;

namespace Sb.Umbraco.Commons.Services
{
    public interface IErrorService
    {
        BaseCommon GetFriendlyErrorPage(int statusCode);
    }
}