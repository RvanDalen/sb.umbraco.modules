﻿using System;
using System.Linq;
using BoC.EventAggregator;
using BoC.Persistence;
using BoC.Services;
using BoC.Validation;
using Sb.Umbraco.Commons.Models.Redirects;
using Sb.Umbraco.Glass.Models;

namespace Sb.Umbraco.Commons.Services
{
    public class RedirectService : BaseModelService<BaseRedirect>, IRedirectService
    {
        private readonly IModelService<BaseCommon> _baseCommonService;

        public RedirectService(IModelValidator validator, IEventAggregator eventAggregator, IRepository<BaseRedirect> repository, IModelService<BaseCommon> baseCommonService) : base(validator, eventAggregator, repository)
        {
            _baseCommonService = baseCommonService;
        }

        public BaseRedirect FindMatch(Uri uri)
        {
            //TODO: make this a bit more flexible.. now we cant have subfolders :/
            var baseFolder = _baseCommonService.Get("/Data/Redirects");
            if (baseFolder == null) return null;

            return baseFolder.Children
                             .OfType<PathRedirect>()
                             .FirstOrDefault(redirect => redirect.UrlPath.Equals(uri.AbsolutePath, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
