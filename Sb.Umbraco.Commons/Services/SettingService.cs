﻿using BoC.EventAggregator;
using BoC.Persistence;
using BoC.Services;
using BoC.Validation;
using Sb.Umbraco.Glass.Models;

namespace Sb.Umbraco.Commons.Services
{
    public class SettingService : BaseModelService<BaseCommon>
    {
        public SettingService(IModelValidator validator, IEventAggregator eventAggregator, IRepository<BaseCommon> repository) : base(validator, eventAggregator, repository)
        {
        }
    }
}
