﻿using BoC.Services;
using Sb.Umbraco.Glass.Models;

namespace Sb.Umbraco.Commons.Services
{
    public class ErrorService : IErrorService
    {
        private readonly IModelService<BaseCommon> _baseCommonService;

        public ErrorService(IModelService<BaseCommon> baseCommonService)
        {
            _baseCommonService = baseCommonService;
        }

        public BaseCommon GetFriendlyErrorPage(int statusCode)
        {
            return _baseCommonService.Get(string.Format("/Home/Error/{0}", statusCode))
                ?? _baseCommonService.Get("/Home/Error");
        }
    }
}
