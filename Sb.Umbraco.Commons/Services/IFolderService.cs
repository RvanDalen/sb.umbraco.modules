using System;
using BoC.Services;
using Sb.Umbraco.Commons.Enums;
using Sb.Umbraco.Commons.Models.Common;
using Sb.Umbraco.Glass.Models;

namespace Sb.Umbraco.Commons.Services
{
    public interface IFolderService : IModelService<FolderData>
    {
        BaseCommon GetOrCreateDateStructure(BaseCommon parent, DateTime dateTime,
            DateFolderStructure dateFolderStructure, string yearFormat = "yyyy", string monthFormat = "MM",
            string dayFormat = "dd");
    }
}