﻿using System;
using System.Linq;
using BoC.EventAggregator;
using BoC.Persistence;
using BoC.Services;
using BoC.Validation;
using Sb.Umbraco.Commons.Enums;
using Sb.Umbraco.Commons.Models.Common;
using Sb.Umbraco.Glass.Models;

namespace Sb.Umbraco.Commons.Services
{
    public class FolderService : BaseModelService<FolderData>, IFolderService
    {
        public FolderService(IModelValidator validator, IEventAggregator eventAggregator, IRepository<FolderData> repository) : base(validator, eventAggregator, repository)
        {
        }

        public BaseCommon GetOrCreateDateStructure(BaseCommon parent, DateTime dateTime,
            DateFolderStructure dateFolderStructure, string yearFormat = "yyyy", string monthFormat = "MM",
            string dayFormat = "dd")
        {
            var parentToReturn = parent;

            //GET OR CREATE YEAR
            if (dateFolderStructure == DateFolderStructure.Year || 
                dateFolderStructure == DateFolderStructure.YearMonth ||
                dateFolderStructure == DateFolderStructure.YearMonthDay)
            {
                var yearFolder = parentToReturn.Children
                                               .OfType<FolderData>()
                                               .FirstOrDefault(folder => folder.Name.Equals(DateTime.Now.ToString(yearFormat)));

                if (yearFolder == null)
                {
                    yearFolder = new FolderData
                    {
                        Name = DateTime.Now.ToString(yearFormat),
                        Parent = parentToReturn
                    };
                    yearFolder = SaveOrUpdate(yearFolder);
                }

                parentToReturn = yearFolder;
            }

            //GET OR CREATE MONTH
            if (dateFolderStructure == DateFolderStructure.YearMonth ||
                dateFolderStructure == DateFolderStructure.YearMonthDay ||
                dateFolderStructure == DateFolderStructure.MonthDay ||
                dateFolderStructure == DateFolderStructure.Month)
            {
                var monthFolder = parentToReturn.Children
                                                .OfType<FolderData>()
                                                .FirstOrDefault(folder => folder.Name.Equals(DateTime.Now.ToString(monthFormat)));

                if (monthFolder == null)
                {
                    monthFolder = new FolderData
                    {
                        Name = DateTime.Now.ToString(monthFormat),
                        Parent = parentToReturn
                    };
                    monthFolder = SaveOrUpdate(monthFolder);
                }

                parentToReturn = monthFolder;
            }

            //GET OR CREATE DAY
            if (dateFolderStructure == DateFolderStructure.YearMonthDay ||
                dateFolderStructure == DateFolderStructure.MonthDay ||
                dateFolderStructure == DateFolderStructure.Day)
            {
                var dayFolder = parentToReturn.Children
                                              .OfType<FolderData>()
                                              .FirstOrDefault(folder => folder.Name.Equals(DateTime.Now.ToString(dayFormat)));

                if (dayFolder == null)
                {
                    dayFolder = new FolderData
                    {
                        Name = DateTime.Now.ToString(dayFormat),
                        Parent = parentToReturn
                    };
                    dayFolder = SaveOrUpdate(dayFolder);
                }

                parentToReturn = dayFolder;
            }

            return parentToReturn;
        }
    }
}
