﻿using BoC.InversionOfControl;
using BoC.Services;
using Sb.Umbraco.Glass.Models;
using umbraco.interfaces;

namespace Sb.Umbraco.Commons.Handlers
{
    public class NotFoundHandler : INotFoundHandler
    {
        private int _redirectId;

        public bool Execute(string url)
        {
            var baseCommonService = IoC.Resolver.Resolve<IModelService<BaseCommon>>();
            var notFoundPage = baseCommonService.Get("/Home/Error/404");

            if (notFoundPage != null)
            {
                _redirectId = notFoundPage.NodeId;
                return true;
            }

            return false;
        }

        public bool CacheUrl
        {
            get { return false; }
        }

        public int redirectID
        {
            get { return _redirectId; }
        }
    }
}
