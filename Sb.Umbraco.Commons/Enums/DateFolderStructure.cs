﻿namespace Sb.Umbraco.Commons.Enums
{
    public enum DateFolderStructure
    {
        YearMonthDay,
        YearMonth,
        Year,
        MonthDay,
        Month,
        Day
    }
}
