﻿namespace Sb.Umbraco.Commons.Enums
{
    public enum HttpStatus
    {
        MovedTemporarily,
        MovedPermenantly
    }
}
